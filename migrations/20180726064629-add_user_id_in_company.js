'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn("companies", "UserId", Sequelize.INTEGER),
            queryInterface.addConstraint('companies', ['UserId'], {
                type: 'foreign key',
                name: 'Companies_UserId_fk_idx',
                references: { // Required field
                    table: 'users',
                    field: 'id'
                },
                onDelete: 'cascade',
                onUpdate: 'cascade'
            })
        ]);
    },
    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeConstraint("companies", "Companies_UserId_fk_idx"),
            queryInterface.removeColumn("companies", "userId")
        ]);
    }
};
