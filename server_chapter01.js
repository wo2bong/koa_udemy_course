const Koa = require('koa');
const app = new Koa();
const PORT = 4000;

const Router = require('koa-router');
const router = new Router();
const bodyParser = require('koa-parser');
const _ = require('lodash');

app.use(bodyParser());

const posts = [
    {
        "id": '1',
        "name": "woobeom 1",
        "content": "content 1"
    }, {
        "id": '2',
        "name": "woobeom 2",
        "content": "content 2"
    }, {
        "id": '3',
        "name": "woobeom 3",
        "content": "content 3"
    }
];

// create a root route
router.get('/', (ctx) => {
    ctx.body = 'welcome to koa application'
});

router.get('/posts', (ctx) => {
    ctx.body = posts;
});

router.get('/posts/:id', (ctx) => {
    ctx.body = posts.find(post => post.id === ctx.params.id);
});

router.post('/posts', (ctx) => {
    // request body를 위해서 koa-parser가 필수
    console.log(ctx.request.body);

    let {id, name, content} = ctx.request.body;

    if (!id) {
        ctx.throw(400, 'id is required');
    }
    if (!name) {
        ctx.throw(400, 'name is required');
    }
    if (!content) {
        ctx.throw(400, 'content is required');
    }

    posts.push({id, name, content});
    ctx.body = posts;
});

router.put('/posts/:id', (ctx) => {
    let {id, name, content} = ctx.request.body;

    const index = posts.findIndex(p => p.id === ctx.params.id);

    if (id) {
        posts[index].id = id;
    }
    if (name) {
        posts[index].name = name;
    }
    if (content) {
        posts[index].content = content;
    }

    ctx.body = posts;
});

router.delete('/posts/:id', (ctx) => {
    ctx.body = _.remove(posts, p => p.id === ctx.params.id);
});

app.use(router.routes());

app.listen(PORT);
console.log(`Server is Lietening on Port ${PORT}`);