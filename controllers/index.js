const CompanyController = require('./CompanyController');
const JobController = require('./JobController');
const ApplicationController = require('./ApplicationController');
const UserController = require('./UserController');

module.exports = {
    CompanyController,
    JobController,
    ApplicationController,
    UserController,
}