module.exports = {
    async create(ctx) {
        try {

            let {title, companyId} = ctx.request.body;

            if (!title) {
                ctx.throw(400, 'please provide the job title');
            }

            if (!companyId) {
                ctx.throw(400, 'please provide the Company Id');
            }

            ctx.body = await ctx.db.Job.create({
                title: title,
                CompanyId: companyId
            });
        } catch (err) {
            ctx.throw(500, err);
        }
    }, async find(ctx) {
        try {

            ctx.body = await ctx.db.Job.findAll({
                include: [
                    {model: ctx.db.Candidate}
                ]
            });
        } catch (err) {
            ctx.throw(500, err);
        }
    },
}