const Util = require('../services/util');
const token = require('../services/token');

module.exports = {
    async signup(ctx) {

        try {
            let {email, password} = ctx.request.body;

            if (!email) {
                ctx.throw(400, 'please provide the email');
            }
            if (!password) {
                ctx.throw(400, 'please provide the password');
            }

            const encryptPassword = await Util.hashPassword(password);
            ctx.body = await ctx.db.User.create({
                email,
                password: encryptPassword
            });

            ctx.status = 200;
            ctx.body = "Signup successful!";

        } catch (err) {
            ctx.throw(500, err);
        }
    },
    async login(ctx) {

        try {
            let {email, password} = ctx.request.body;

            if (!email) {
                ctx.throw(400, 'please provide the email');
            }
            if (!password) {
                ctx.throw(400, 'please provide the password');
            }

            const user = await ctx.db.User.findOne({
                where: {email}
            });

            if (!user) {
                ctx.throw(500, 'unable to process request');
            }

            const matched = Util.comparedPassword(password, user.password);
            if (matched) {
                const jwtToken = token.issue({
                    payload: {
                        user: user.id
                    }
                }, '1 day');

                ctx.body = {"token": jwtToken};
            } else {
                ctx.throw(500, 'invalid password');
            }

        } catch (err) {
            ctx.throw(500, err);
        }
    }
}