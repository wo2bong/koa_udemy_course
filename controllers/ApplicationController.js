module.exports = {
    async create(ctx) {
        try {

            let {firstName, lastName, email, jobId} = ctx.request.body;

            const candidate = await ctx.db.Candidate.create({
                firstName: firstName,
                lastName: lastName,
                email: email,
            });

            ctx.body = await ctx.db.Application.create({
                JobId: jobId,
                CandidateId: candidate.id,
            });

        } catch (err) {
            ctx.throw(500, err);
        }
    }
}