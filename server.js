const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-parser');

const router = require('./routes');

const app = new Koa();
const PORT = 4000;

const db = require('./models');

// sync parameter {force: true} 면 drop table 후 create 한다.
// {force: false, }
db.sequelize.sync()
    .then(() => console.log('models Synced!'))
    .catch(err => console.log(err));

app.context.db = db;
app.use(bodyParser());
app.use(router.routes());

app.listen(PORT);

console.log(`Server is listening on PORT ${PORT}`);
